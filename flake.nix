{
  description = "Just Another Elm Site Generator";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    haskellNix.url = "github:input-output-hk/haskell.nix";
  };

  outputs = { self, nixpkgs, utils, haskellNix }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
      haskellNix' = haskellNix.legacyPackages."${system}";

      project = haskellNix'.haskell-nix.project {
        src = ./.;
      };
    in rec {
      # `nix build`
      packages.jaesg = pkgs.runCommand "jaesg" {
        nativeBuildInputs = with pkgs; [ makeWrapper ];
      } ''
        cp -r "${project.jaesg.components.exes.jaesg}" "$out"
        chmod +w "$out/bin"
        wrapProgram "$out/bin/jaesg" --set ELM "${pkgs.elmPackages.elm}/bin/elm" --set UGLIFYJS "${pkgs.nodePackages.uglify-js}/bin/uglifyjs"
      '';
      defaultPackage = packages.jaesg;

      # `nix develop`
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [];
        nativeBuildInputs = with pkgs; [ stack ];
      };

      # `nix run`
      apps.jaesg = utils.lib.mkApp {
        name = "jaesg";
        drv = packages.jaesg;
      };
      defaultApp = apps.jaesg;
    });
}
