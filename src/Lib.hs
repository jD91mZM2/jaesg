{-# LANGUAGE TemplateHaskell #-}

module Lib
    ( Mode(Mode)
    , main
    , modeParser
    , dryRun
    , production
    ) where

import Control.Concurrent.Async
import Control.Monad
import Data.FileEmbed
import Data.List
import Data.Maybe
import Options.Applicative
import System.Directory
import System.Environment
import System.FilePath
import System.Process
import Text.HTML.TagSoup

-- Compilation options and CLI parser

data Mode = Mode
  { production :: Bool,
    dryRun :: Bool
  }
  deriving (Show)

modeParser :: Parser Mode
modeParser = Mode
  <$> switch
    ( long "prod"
    <> help "Whether to run in production mode" )
  <*> switch
    ( long "dry-run"
    <> help "If set, nothing will actually be built" )

-- Utilities

runElm :: [String] -> Mode -> IO ()
runElm args mode | dryRun mode = putStrLn $ intercalate " " $ "$ELM":args
runElm args _ = do
  elm <- fromMaybe "elm" <$> lookupEnv "ELM"
  callProcess elm args

runUglify :: [String] -> Mode -> IO ()
runUglify args mode | dryRun mode = putStrLn $ intercalate " " $ "$UGLIFYJS":args
runUglify args _ = do
  elm <- fromMaybe "uglifyjs" <$> lookupEnv "UGLIFYJS"
  callProcess elm args

compile :: FilePath -> FilePath -> Mode -> IO ()
compile input output mode = runElm ["make", input, "--output", output, if (production mode) then "--optimize" else "--debug"] mode

minify :: FilePath -> Mode -> IO ()
minify input mode | production mode = do
  runUglify [input, "--compress", "pure_funcs=\"F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9\",pure_getters,keep_fargs=false,unsafe_comps,unsafe", "--output", input] mode
  runUglify [input, "--mangle", "--output", input] mode
minify input _ = pure ()

ensureDir :: FilePath -> Mode -> IO ()
ensureDir path mode | dryRun mode = putStrLn $ "Ensuring directory " ++ show path ++ " exists"
ensureDir path _ = createDirectoryIfMissing True path

saveFile :: FilePath -> String -> Mode -> IO ()
saveFile path source mode | dryRun mode = putStrLn $ "Writing file " ++ show path
saveFile path source _ = writeFile path source

outputName :: String -> FilePath -> FilePath
outputName ext path = if takeFileName (takeDirectory path) == "Main"
                      then replaceFileName (takeDirectory path) "index" ++ ext
                      else replaceExtension (takeDirectory path) ext

-- Recurse directories and look for elm files to act on.

findElmFiles :: FilePath -> FilePath -> Mode -> IO [(FilePath, FilePath)]
findElmFiles inputDir outputDir mode = do
  let srcpath = (inputDir </>)
  let dstpath = (outputDir </>)

  entries <- listDirectory inputDir
  dirs    <- filterM (doesDirectoryExist . srcpath) entries
  files   <- filterM (doesFileExist . srcpath) entries

  childEntries <- concat <$> mapM (\child -> findElmFiles (srcpath child) (dstpath child) mode) dirs :: IO [(FilePath, FilePath)]

  let elm = filter (\file -> takeFileName file == "Page.elm") files :: [FilePath]
  let elmEntries = map (\file -> (srcpath file, dstpath file)) elm :: [(FilePath, FilePath)]

  pure (elmEntries ++ childEntries)

-- Functions to generate HTML

defaultTemplate :: String
defaultTemplate = $(embedStringFile "template.html")

traverseUpwards :: FilePath -> FilePath -> String -> IO (Maybe FilePath)
traverseUpwards root dir filename = do
  exists <- doesFileExist (dir </> filename)
  if exists
    then pure (Just (dir </> filename))
    else if dir `equalFilePath` root
         then pure Nothing
         else traverseUpwards root (takeDirectory dir) filename

sourceHtmlFor :: FilePath -> FilePath -> IO String
sourceHtmlFor inputDir input = do
  filePath <- traverseUpwards inputDir (takeDirectory input) "template.html"
  maybe (pure defaultTemplate) readFile filePath

scriptFor :: FilePath -> FilePath -> [Tag String]
scriptFor inputDir input =
  let
    relative = makeRelative inputDir input

    parts = map dropTrailingPathSeparator $ splitPath $ dropExtension relative
    moduleName = intercalate "." parts
  in
    [ TagComment "start injected script"

    , TagOpen "script" [("src", "/" ++ outputName ".js" relative)]
    , TagClose "script"

    -- Injecting the basename of the file is safe because Elm
    -- already validates the filename. Also if the user wants to
    -- have weird filenames and manage to break this, it's not
    -- like its affecting anyones security...
    , TagOpen "script" []
    , TagText $ "let ElmApp = Elm." ++ moduleName ++ ";"
    , TagClose "script"

    , TagComment "end injected script"
    ]

injectScript :: String -> [Tag String] -> String
injectScript source script = let
  tags = parseTags source
  (head, rest) = break (~== "<body>") tags
  injected = case rest of
    (body:tail) -> head ++ [body] ++ script ++ tail
    [] -> head
  in
    renderTags injected

genHtmlFor :: FilePath -> FilePath -> IO String
genHtmlFor inputDir input = do
  source <- sourceHtmlFor inputDir input
  pure $ injectScript source (scriptFor inputDir input)

-- Main function that combines all forms of compilations and generations

handle :: FilePath -> FilePath -> FilePath -> Mode -> IO ()
handle inputDir input output mode = do
  let htmlpath = outputName ".html" output
  let jspath = outputName ".js" output

  ensureDir (takeDirectory htmlpath) mode
  compile input jspath mode
  minify jspath mode

  html <- genHtmlFor inputDir input
  saveFile htmlpath html mode


main :: FilePath -> FilePath -> Mode -> IO ()
main inputDir outputDir mode = do
  files <- findElmFiles inputDir outputDir mode
  let f = \(input, output) -> handle inputDir input output mode
  if production mode
    then mapConcurrently f files >> pure ()
    else mapM_ f files
