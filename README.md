# jaesg - Just Another Elm Site Generator

A simple way to convert your elm sites into webpages.

## Usage

Your elm structure must look similar to this:

```
src/
+--- Main/
|    +---- Page.elm      # "Main" is special-cased and becomes "index.html"
+--- Secondary/
|    +---- Page.elm      # becomes Secondary.html
|    +---- template.html # affects Secondary.html and all subdirectories
+--- Third/
|    +---- Page.elm      # becomes Third.html
|    +---- Util.elm      # ignored
```

Just run `jaesg <source directory>` and it will recursively compile your
`Page.elm` Elm files and generate HTMLs.

If you want a custom HTML template (you probably do), you can do it per-subpath
or per-project. Say jaesg is compiling `Counter/Page.elm`. It will first look
for `src/Test/Counter/template.html` - if it finds it, it will use that as a
template. If not, it will look for `src/Test/template.elm`. Finally, it will
look in `src/template.elm`. If all of them are missing, a sane default is used.

The HTML template is used as-is, but a script tag is automatically injected at
the start of the `<body>` which will load the Elm script and create a variable
`ElmApp` which will refer to `Elm.<module name>` for your page.

``` html
<!DOCTYPE html>
<html>
    <head>
        <title>My Page</title>
    </head>
    <body>
        <!--
          script automagically injected here so ElmApp is set to the current
          module
          -->
        <div id="myapp" />
        <script>
         ElmApp.init({
             node: document.getElementById("myapp"),
         })
        </script>
    </body>
</html>
```

## Example

Try entering the `dummy-pages/` directory and run

``` sh
jaesg src/
```

then you can serve the default build directory using

``` sh
python -m http.server --directory build/
```

## Reasons to make another

1. It was fun to make
1. Almost all alternatives are written in fucking JavaScript...
1. I have a severe allergy against npm

## Alternatives

I tried to use [elm-pages](https://github.com/dillonkearns/elm-pages), but as I
ran `npm installed` I saw warnings about "old insecure binaries" and then it
tried to download binaries of the entire Chromium project. Now, I don't know
about you, but I don't like downloading binaries that some random idiot has
pre-compiled on their system. I only want to run binaries that a CI has compiled
and you can inspect the logs for - because I don't find it very fun to give
random people access to my entire computer. So while I accept that the project
was using outdated libraries, I don't accept that the libraries try to shove
their stupid binaries onto people.

Nevertheless, once you actually get them installed, I'm sure
[elm-pages](https://github.com/dillonkearns/elm-pages) and/or
[elmstatic](https://korban.net/elm/elmstatic/) and/or
[elm-static-site](https://github.com/eeue56/elm-static-site) are lovely
projects. But I didn't want to use them, because the second I thought "npm can't
be that fucking bad" it tried to infect my system with those pesky binaries.

But from my perspective, this project using Haskell is absolute gold.
