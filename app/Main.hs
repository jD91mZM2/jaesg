module Main where

import Lib (Mode)
import qualified Lib (modeParser, main, dryRun)

import System.Directory
import Options.Applicative

data Opt = Opt
  { input :: String,
    output :: String,
    mode :: Mode
  }
  deriving (Show)

data Arg = String Opt

optParser :: Parser Opt
optParser = Opt
  <$> argument str
    ( metavar "INPUT"
    <> help "Source directory containing Main.elm" )
  <*> argument str
    ( metavar "OUTPUT"
    <> help "Destination directory name, filled with static html pages"
    <> value "build" )
  <*> Lib.modeParser

main :: IO ()
main = execParser (info (optParser <**> helper) (fullDesc)) >>= entry

entry :: Opt -> IO ()
entry opts = do
  -- Remove previous build
  exists <- doesDirectoryExist (output opts)
  if exists && not (Lib.dryRun $ mode opts)
    then removeDirectoryRecursive (output opts)
    else pure ()

  -- Rebuild
  Lib.main (input opts) (output opts) (mode opts)
